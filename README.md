## Frontend Test

Para rodar o projeto basta:

- Clonar ou baixar o projeto

- Entrar na pasta onde clonou ou baixou os arquivos

- No Terminal, entre na pasta do projeto e rode o comando 'npm install' no terminal

- Apos instalado, rode o comando 'npm start' 

- No browser, entre em http://localhost:3000/ ou http://localhost:3000/produtos


## Considerações

O projeto levou cerca de 6h30 para ser feito.

Não tive tempo para fazer algumas coisas que gostaria no projeto, como:

- Colocar um loading para o carregamento da lista e dos dados do produto

- Esconder o botão 'Continuar lendo' caso a descrição caiba totalmente na página
