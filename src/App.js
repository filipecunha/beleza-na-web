import React, { Component } from 'react'

import './App.scss'
import Route from './Route'

class _App extends Component {

    render() {
        return (
            <div className="App">
                <Route/>
            </div>
        )
    }
}

export const App = _App
