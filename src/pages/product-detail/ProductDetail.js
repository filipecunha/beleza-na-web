import React from 'react';
import { connect } from 'react-redux';
import NumberFormat from 'react-number-format';
import ReactHtmlParser from 'react-html-parser';

import './product-detail.scss';
import { fetchProduct, resetProductInfos } from '../../redux/products/actions';
import Button from '../../components/button/Button';

class _ProductDetail extends React.Component {

    state = {
        isDescriptionOpened: false,
    }

    componentDidMount() {
        this.props.fetchProduct(this.props.match.params.id);
    };

    componentWillUnmount() {
        this.props.resetProductInfos();
    }

    onClickContinueRead = () => {
        this.setState({isDescriptionOpened: true });
    };

    render () {
        if(this.props.productData.sku) {
            const productData = this.props.productData;

            return (
                <div className="product-detail">
                    <h2>Detalhes do produto</h2>
                    <div className={`detail-wrapper ${this.state.isDescriptionOpened ? 'detail-wrapper--opened' : ''}`}>
                        <h3>{productData.name}</h3>
                        <img className="product-image" src={productData.imageObjects[0].large} alt=""/>
                        <div className="payment-and-code">
                            <div className="payment">
                                {productData.priceSpecification.maxPrice !== productData.priceSpecification.price &&
                                <div className="product-old-price">
                                    <NumberFormat
                                        value={productData.priceSpecification.maxPrice}
                                        displayType="text"
                                        defaultValue="number"
                                        decimalSeparator=","
                                        decimalScale={2}
                                        fixedDecimalScale={true}
                                        allowLeadingZeros={true}
                                        prefix={'R$ '}
                                    />
                                </div>
                                }
                                <div className="product-price">
                                    <NumberFormat
                                        value={productData.priceSpecification.price}
                                        displayType="text"
                                        defaultValue="number"
                                        decimalSeparator=","
                                        decimalScale={2}
                                        fixedDecimalScale={true}
                                        allowLeadingZeros={true}
                                        prefix={'R$ '}
                                    />
                                </div>
                                <div className="instalment">
                                    {productData.priceSpecification.installments.numberOfPayments}x
                                    <NumberFormat
                                        value={productData.priceSpecification.installments.monthlyPayment}
                                        displayType="text"
                                        defaultValue="number"
                                        decimalSeparator=","
                                        decimalScale={2}
                                        fixedDecimalScale={true}
                                        allowLeadingZeros={true}
                                        prefix={' R$ '}
                                    />
                                </div>
                            </div>
                            <div className="code">
                                <div className="product-brand">{productData.brand.name}</div>
                                <div className="product-code">cod: {productData.sku}</div>
                            </div>
                        </div>
                        <div className="button-wrapper">
                            <Button className="buy-button" label="COMPRE" type="primary" />
                        </div>
                        <div className="product-description">
                            <h4>Descrição do Produto</h4>
                            <div className="description">{
                                ReactHtmlParser(productData.details.description)
                            }</div>
                        </div>
                        {!this.state.isDescriptionOpened &&
                        <div
                            className="continue-read"
                            onClick={this.onClickContinueRead}
                        >
                            Continuar Lendo
                        </div>
                        }
                    </div>
                </div>
            )
        } else {
            return <div></div>
        }
    }

}

const mapStateToProps = (state) => {
    return {
        productData: state.products.productData,
    }
}

export const ProductDetail = connect(mapStateToProps, {fetchProduct, resetProductInfos})(_ProductDetail);
