import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import ProductCard from '../../components/product-card/ProductCard';

import './product-list.scss';
import Button from '../../components/button/Button';
import { fetchProducts } from '../../redux/products/actions'

const _ProductList = (props) => {

    const [page, setPage] = useState(1);

    useEffect (() => {
        loadPage();
    },[]);

    const loadPage = async () => {
        await props.fetchProducts(page);
        setPage(page + 1);
    }

    return (
        <div className="product-list">
            <h2>Lista de produtos</h2>
            <div className="cards-list" onClick={loadPage}>
                {
                    props.productsData.map((product, index) => {
                        return <ProductCard
                            key={product.sku}
                            id={product.sku}
                            image={product.imageObjects[0].large}
                            description={product.details.shortDescription}
                            price={product.priceSpecification.price}
                            maxPrice={product.priceSpecification.maxPrice}
                            history={props.history}
                        />
                    })
                }
            </div>
            <Button
                label="Carregar mais produtos"
                type="secondary"
                onClick={loadPage}
            />
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        productsData: state.products.productsData,
    }
}

export const ProductList = connect(mapStateToProps, { fetchProducts })(_ProductList)
