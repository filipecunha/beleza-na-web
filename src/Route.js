import React from 'react'
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'

import {
    ProductList,
    ProductDetail
} from './pages'

export default () => (
    <div>
        <Router>
            <Switch>
                <Route path="/" exact component={ProductList}/>
                <Route path="/produtos" component={ProductList}/>
                <Route path="/produto/:id" component={ProductDetail}/>
            </Switch>
        </Router>
    </div>
);
