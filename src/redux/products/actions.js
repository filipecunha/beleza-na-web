import axios from 'axios';

import { ACTION_TYPE } from './actionTypes'
import { CORS } from '../../utils/api';

export const fetchProducts = (page) => async dispatch => {
    const response = await axios.get(`${CORS}https://pacific-wave-51314.herokuapp.com/products?page=${page}&size=10`)

    dispatch({
        type: ACTION_TYPE.FETCH_PRODUCTS,
        payload: response.data
    })

}

export const fetchProduct = (sku) => async dispatch => {
    const response = await axios.get(`${CORS}https://pacific-wave-51314.herokuapp.com/products/${sku}`)

    dispatch({
        type: ACTION_TYPE.FETCH_PRODUCT,
        payload: response.data
    })
}

export const resetProductInfos = (sku) => async dispatch => {

    dispatch({
        type: ACTION_TYPE.RESET_PRODUCT_INFOS
    })
}
