import { ACTION_TYPE } from './actionTypes'

const initialState = {
    productsData: [],
    productData: {},
}

export default (state = initialState, action) => {
    switch (action.type) {

        case ACTION_TYPE.FETCH_PRODUCTS: {
            return {
                ...state,
                productsData: [...state.productsData, ...action.payload]
            }
        }

        case ACTION_TYPE.FETCH_PRODUCT: {
            return {
                ...state,
                productData: action.payload
            }
        }

        case ACTION_TYPE.RESET_PRODUCT_INFOS: {
            return {
                ...state,
                productData: {}
            }
        }

        default:
            return state
    }
};
