import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'

import products from './products/reducer'

const reducers = combineReducers({
    products
})

export default createStore(reducers, applyMiddleware(thunk))
