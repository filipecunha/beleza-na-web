import React from 'react'
import PropTypes from 'prop-types'

import './button.scss'

const _Button = ({ className, type, label, onClick, style }) => {
    return (
        //eslint-disable-next-line
        <div
            className={`button-component ${type === 'primary' ? 'button-component--primary' : 'button-component--secondary'} ${className}`}
            style={style}
            onClick={onClick}
        >
            {label}
        </div>
    )
}

_Button.propTypes = {
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    style: PropTypes.object,
}

export default _Button
