import React, { Component } from 'react'
import NumberFormat from 'react-number-format';
// import PropTypes from 'prop-types'

import './product-card.scss'
import Button from '../button/Button'

class ProductCard extends Component {
    onClickDetails = () => {
        this.props.history.push(`/produto/${this.props.id}`);
    }

    render() {
        return (
            <div className="product-card">
                <div className="left-column">
                    <img className="product-image" src={this.props.image} alt=""/>
                    <div className="product-code">cod: {this.props.id}</div>
                </div>
                <div className="right-column">
                    <p className="product-text">{this.props.description}</p>
                    <div className="price-wrapper">
                        {this.props.maxPrice !== this.props.price &&
                            <div className="product-old-price">
                                <NumberFormat
                                    value={this.props.maxPrice}
                                    displayType="text"
                                    defaultValue="number"
                                    decimalSeparator=","
                                    decimalScale={2}
                                    fixedDecimalScale={true}
                                    allowLeadingZeros={true}
                                    prefix={'R$ '}
                                />
                            </div>
                        }
                        <div className="product-price">
                        {
                            <NumberFormat
                                value={this.props.price}
                                displayType="text"
                                defaultValue="number"
                                decimalSeparator=","
                                decimalScale={2}
                                fixedDecimalScale={true}
                                allowLeadingZeros={true}
                                prefix={'R$ '}
                            />
                        }
                        </div>
                    </div>
                    <div
                        className="button-wrapper"
                        onClick={this.onClickDetails}
                    >
                        <Button
                            type="primary"
                            label="Ver detalhes"
                        />
                    </div>
                </div>
            </div>
        )
    }
}

// ProductCard.propTypes = {}

export default ProductCard
